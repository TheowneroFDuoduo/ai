/**
 * 小程序配置文件
 */
// 此处主机域名是软云小程序解决方案分配的域名
// 小程序后台服务解决方案：https://yuanruan.softseeks.cn

// 下面的地址配合云端 Server 工作
var config = {
  queryupload: '/skinapi/upload',//文件上传
  querygetSkinReport: '/skinapi/getSkinReport', //测肤报告接口
  querygetPayState: '/skinapi/getPayState',//获取支付状态接口 
  querygetRecommendGoods: '/goods/getRecommendGoods',//获取商品分类
  GetProductcenterList: '/Commoditymanagement/QueryGoodsListByGoodscategoryid',//根据商品分类id获取其下商品数据
  queryOrCreateMembership: '/Member/queryOrCreateMembership',//获取登录用户
  GetArticle: '/Enterprisewebsite/GetArticle',//获取文章详细
  GetArticleListByEncode: '/Enterprisewebsite/GetArticleListByEncode',//获取分类Code获取数据
  GetArticleListByCode: '/Enterprisewebsite/GetArticleListByCode',//获取分类Id获取数据
  GetGuntuwoArticle: '/Enterprisewebsite/GetGuntuwoArticle',//根据Code获取详细信息
  GetProductcenter: '/Enterprisewebsite/GetProductcenter',//获取产品详细
  AddOnlinecontact: '/Enterprisewebsite/AddOnlinecontact',//预约
  queryGoodDetitle: '/Good/queryGoodDetitle',//商品详细
  queryUserfavorite: '/Good/queryUserfavorite',//取消和收藏商品
  queryAddShoppCart: '/Good/queryAddShoppCart',//添加购物车
  queryGetShoppCartlist: '/Good/queryGetShoppCartlist',//获取购物车
  queryDeleteShoppCart: '/Good/queryDeleteShoppCart',//删除购物车
  queryAddShippingaddress: '/Member/queryAddShippingaddress',//新增与修改收货地址
  queryShippingaddress: '/Member/queryShippingaddress',//获取收货地址详细
  queryShippingaddressList: '/Member/queryShippingaddressList',//获取收货地址详细
  queryDeleteShippingaddress: '/Member/queryDeleteShippingaddress',//删除收货地址
  queryIsdefaultaddress: '/Member/queryIsdefaultaddress',//默认收货地址
  querycreateorder: '/Order/querycreateorder',//提交订单
  queryupdateorder: '/Order/Paysuccess',//更新订单POST /api/Order/
  queryorderlist: '/Order/queryorderlist',//查询订单
  queryorder: '/Order/queryorder',//查询订单信息
  queryyouxiaoUsercoupons: '/Order/queryyouxiaoUsercoupons',//查询出满足当前订单的优惠券
  querydeleteorder: '/Order/querydeleteorder',//查询订单信息
  queryMycollection: '/Good/queryMycollection',//查询用户收藏的商品
  queryVendorsendsms: '/Member/queryVendorsendsms',//发送短信验证码
  queryupdatemodebile: '/Member/queryupdatemodebile',//绑定手机号码
  queryUsercouponslist: '/Member/queryUsercouponslist',//绑定手机号码
  queryGoodlistlike: '/Good/queryGoodlistlike',//查询商品列表
  queryorderreceiving: '/Order/queryorderreceiving',//我要收货
  queryMycomment: '/Order/queryGoodcomment',//我要评价
  queryOrderGood: '/Order/queryOrderGood',//根据子订单ID获取商品信息以及订单信息
  queryOrderrefund: '/Order/queryOrderrefund',//申请售后
  queryOrderrefundGoodlist: '/Order/queryOrderrefundGoodlist',//申请售后订单
  queryOrderrefundGood: '/Order/queryOrderrefundGood',//申请售后订单详情
  queryaddVendorcoupons: '/Good/queryaddVendorcoupons',//领取优惠券
  queryWxpayorder: '/Order/queryWxpayorder',//微信支付
  queryGoodcomment: '/Good/queryGoodcomment',//获取商品评价
  queryVendorcouponsList: '/Good/queryVendorcouponsList',//获取商家优惠券
  GetServiceorder: '/Order/GetServiceorder',//服务订单页面获取订单信息
  SubmitServiceorder: '/Order/SubmitServiceorder',//服务订单页面获取订单信息
  GetServiceorderDetitle: '/Order/GetServiceorderDetitle',//获取服务订单详细信息
  modifyMembership: '/Member/modifyMembership',//修改用户信息

};
module.exports = config
