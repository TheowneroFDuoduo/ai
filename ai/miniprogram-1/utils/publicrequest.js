var server = require('config');
var parser = require('../Lib/dom/dom-parser.js');
const app = getApp()
//公众请求
function softseeksrequest(data, requestpost, method, success) {
  wx.request({
    url: server.requestindexUrl + requestpost,
    data: data,
    method: method,
    header: {
      'content-type': 'application/json',
      'AppID': server.Appid,
      'access-key': server.accesskey,
      'Vendor': server.Vendor
    },
    success: function (res) {
      if (res.statusCode == 200 && !res.data.result_code) {
       // var data = JSON.parse(res.data)
        typeof success == "function" && success(res.data);
      } else {
        typeof fail == "function" && fail(res);
      }
    },
    fail: function (reresss) {
      console.log("reresss")
    }
  })
}

//公众请求
function sofGoodksrequest(datalist, GoodlistJosn, requestpost, method, success) {
  var datalist12 = JSON.stringify(datalist);
  var GoodlistJosn12 = JSON.stringify(GoodlistJosn);

  wx.request({
    url: server.requestindexUrl + requestpost + '?datalist=' + datalist12 + "&GoodlistJosn=" + GoodlistJosn12,
    data: { datalist: datalist12, GoodlistJosn: GoodlistJosn12 },
    method: method,
    header: {
      'content-type': 'application/json',
      'AppID': server.Appid,
      'access-key': server.accesskey,
      'Vendor': server.Vendor
    },
    success: function (res) {
      if (res.statusCode == 200 && !res.data.result_code) {
        var data = JSON.parse(res.data)
        typeof success == "function" && success(data);
      } else {
        typeof fail == "function" && fail(res);
      }
    },
    fail: function (reresss) {
      console.log("reresss")
    }
  })
}
//推送消息
function orderSign(){
  
}
//文件上传
function upload_file(tempFilePath, Account, requestpost, success,fail) {
  console.log(app.globalData.adress)
  var xmlparser = new parser.DOMParser();
  wx.uploadFile({
    url: server.requestindexUrl + requestpost,
    filePath: tempFilePath,
    name: 'file',
    header: {
      'content-type': 'multipart/form-data'
    },
    formData: {
      // 'file': tempFilePath
      'content-type': 'multipart/form-data',
      machineId:(app.globalData.adress[0]+'')+(app.globalData.adress[1]+''),
      latitude:app.globalData.adress[0],
      longitude:app.globalData.adress[1]
    },
    success: function (res) {
      console.log(res,'拍照公共方法')
      var data = JSON.stringify(res.data);
      data = JSON.parse(res.data);
      if (data.code == 200) {
        typeof success == "function" && success(data);
      } else {
        typeof fail == "function" && fail(data);
      }
    },
    fail: function (res) {
      console.log(res,55);
    }
  })
}

module.exports = {
  softseeksrequest: softseeksrequest,
  sofGoodksrequest: sofGoodksrequest,
  upload_file: upload_file
}