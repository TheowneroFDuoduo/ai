// pages/goods/goods.js
const app = getApp()
var config = require('../../utils/config');
import drawQrcode from '../../Lib/weapp.qrcode.min.js'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    showtc: false,
    topNum:0,
    tabCur: 0, //默认选中
    goodlist: [],
    pageNum: 1,
    tabs: [],
    id: null,
    ido: null,
    total0: 0,
    totalf0: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.hideShareMenu()// 禁用分享
    var arr = [];
    for (let i in options) {
      arr.push(options[i])
    }
    const id = arr[0];
    this.setData({
      id: id,
      ido: id
    });
    var that = this
    wx.request({
      url: config.host + '/goods/getGoodsClassify',
      data: {},
      method: 'GET',
      success: function (res) {
        that.setData({
          tabs: res.data.data
        })
      }
    })
    wx.request({
      url: config.host + '/goods/getRecommendGoods',
      data: {
        asc: true,
        orderFiled: "id",
        pageNum: that.data.pageNum,
        pageSize: 8,
        reportId: id
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          goodlist: res.data.data.records,
          total0: res.data.data.total
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},
  hiddenwxpay() {
    this.setData({
      showtc: false
    });
  },
  draw(adress) {
    drawQrcode({
      x: 15,
      y: 30,
      width: 490,
      height: 490,
      canvasId: 'myQrcode',
      // ctx: wx.createCanvasContext('myQrcode'),
      typeNumber: 10,
      text: adress,
      //   image: {
      //     imageResource: '../../images/icon.png',
      //     dx: 70,
      //     dy: 70,
      //     dWidth: 60,
      //     dHeight: 60
      //   },
      callback(e) {}
    })
  },
  wxbyadress(e) {
    this.setData({
      showtc: true
    });
    console.log(e.currentTarget.dataset.link)
    this.draw(e.currentTarget.dataset.link)
  },
  //选择条目
  tabSelect(e) {
    var that = this
    this.setData({
      tabCur: e.currentTarget.dataset.index,
      scrollLeft: (e.currentTarget.dataset.index - 2) * 50,
      pageNum: 1,
      goodlist:[]
    })
    if (e.currentTarget.dataset.index == 0) {
      wx.request({
        url: config.host + '/goods/getRecommendGoods',
        data: {
          asc: true,
          orderFiled: "id",
          pageNum: 1,
          pageSize: 7,
          reportId: that.data.ido
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            goodlist: res.data.data.records,
            id: e.currentTarget.dataset.id,
            total0: res.data.data.total
          })
        }
      })
    } else {
      wx.request({
        url: config.host + '/goods/getGoodsByType',
        data: {
          asc: true,
          orderFiled: "id",
          pageNum: 1,
          pageSize: 7,
          typeId: e.currentTarget.dataset.id
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            goodlist: res.data.data.records,
            id: e.currentTarget.dataset.id,
            totalf0: res.data.data.total
          })
        }
      })
    }
    // 重新请求商品列表数据
    // 请求数据跟新 goodlist
    this.setData({
      topNum:0
    }) 
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this
    console.log('下拉刷新', this.data.tabCur)
    if (this.data.tabCur == 0) {
      wx.request({
        url: config.host + '/goods/getRecommendGoods',
        data: {
          asc: true,
          orderFiled: "id",
          pageNum: 1,
          pageSize: 7,
          reportId: that.data.ido
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            goodlist: res.data.data.records
          })
        }
      })
    } else {
      wx.request({
        url: config.host + '/goods/getGoodsByType',
        data: {
          asc: true,
          orderFiled: "id",
          pageNum: 1,
          pageSize: 7,
          typeId: that.data.id
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            goodlist: res.data.data.records
          })
        }
      })
    }
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var arr = this.data.goodlist
    var that = this
    console.log('上拉加载', this.data.tabCur, that.data.total0, that.data.pageNum)
    if (this.data.tabCur == 0 && (that.data.pageNum < that.data.total0)) {
      wx.request({
        url: config.host + '/goods/getRecommendGoods',
        data: {
          asc: true,
          orderFiled: "id",
          pageNum: ++that.data.pageNum,
          pageSize: 7,
          reportId: that.data.ido
        },
        method: 'POST',
        success: function (res) {
          console.log(res)
          arr = arr.concat(res.data.data.records);
          that.setData({
            goodlist: arr
          })
        }
      })
    } else if (that.data.pageNum < that.data.totalf0) {
      wx.request({
        url: config.host + '/goods/getGoodsByType',
        data: {
          asc: true,
          orderFiled: "id",
          pageNum: ++that.data.pageNum,
          pageSize: 7,
          typeId: that.data.id
        },
        method: 'POST',
        success: function (res) {
          console.log(res)
          arr = arr.concat(res.data.data.records);
          that.setData({
            goodlist: arr
          })
        }
      })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})