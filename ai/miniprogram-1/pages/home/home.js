// pages/home/home.js
const app = getApp()
var Service = require('../../utils/Service');
var publicrequest = require('../../utils/publicrequest.js');
var config = require('../../utils/config');
var times = 0
Page({
  /**
   * 页面的初始数据
   */
  data: {
    contenttxt: '照片不正确,请重新上传',
    device: 'front',
    isFlash: "off",
    showCamera: true,
    id: 0,
    modules: [{
      img: '../../img/photoFrame.png', //可以选择的模版
      showImg: '../../img/photoFrame.png', //在拍照的时候的图片
      isSelected: false
    }],
    showCanvas: '',
    windowWidth: 0,
    windowHeight: 0,
    step: "0",
    chooseImg: "", //选择的模版
    step2Canvas: "",
    photoPath: "", //拍的照片的地址
    text: "",
    showText: true,
    left: 0,
    top: 0,
    startX: 0,
    startY: 0,
    num: 3,
    opentc: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu() // 禁用分享
    if (options.opentc) {
      this.setData({
        opentc: options.opentc,
        contenttxt: options.txt
      })
    }
    this.getSystem()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this
    this.selectModule()
    //获得popup组件
    this.popup = this.selectComponent("#popup");
  },
  showPopup() {
    this.popup.showPopup();
  },

  //取消事件
  _error() {
    console.log('你点击了取消');
    this.popup.hidePopup();
  },
  //确认事件
  _success() {
    console.log('你点击了确定');
    this.popup.hidePopup();
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if(!app.globalData.adress){
      wx.getLocation({
        success(res) {
          // 2.2 获取用户位置成功后，将会返回 latitude, longitude 两个字段，代表用户的经纬度位置
          // 2.3 将获取到的 经纬度传值给 getAddress 解析出 具体的地址
         app.globalData.adress=[res.latitude, res.longitude]
         console.log(app.globalData.adress,res.latitude,99)
        }
      })
    }
    times = 0
    var that = this
    this.data.showCanvas = wx.createCanvasContext("show");
    if (this.data.opentc == 1) {
      setTimeout(function () {
        that.popup.showPopup();
      }, 1200)
    }
  },
  getSystem() {
    let that = this;
    wx.getSystemInfo({
      success: function (res) {
        console.log(res,666)
        that.setData({
          windowWidth: res.windowWidth,
          windowHeight: res.windowHeight
        })
      },
    })
  },
  //选择模版
  selectModule(e) {
    this.data.modules.forEach(item => {
      item.isSelected = false;
    })
    let item = {
      img: '../../img/photoFrame.png', //可以选择的模版
      showImg: '../../img/photoFrame.png', //在拍照的时候的图片
      isSelected: false
    }
    this.data.chooseImg = item.showImg
    let isSelected = `modules[0].isSelected`;
    this.setData({
      [isSelected]: true,
      modules: this.data.modules,
      chooseImg: this.data.chooseImg
    })
    this.data.showCanvas.drawImage(this.data.chooseImg, (this.data.windowWidth - 256) / 2, (920 - 283) / 2, 256, 283)
    this.data.showCanvas.draw()
  },
  daojishi() {
    var that = this
    var timer = setInterval(function () {
      if (that.data.num > 1) {
        let t = that.data.num - 1
        that.setData({
          num: t
        })
      } else {
        clearInterval(timer);
      }
    }, 1000);
  },
  // 开启倒计时超时调用和拍照延迟调用。刚好数完就拍照
  startcamara() {
    var that = this
    this.setData({
      step: '1'
    })
    setTimeout(function () {
      that.takePhoto()
    }, 3100) //数完321开拍
    that.daojishi()
  },
  //拍照
  takePhoto() {
    console.log('显示拍照组件提示，若没有开启权限，提示调用拍照。确认开始倒计时拍照')
    const ctx = wx.createCameraContext();
    let that = this
    ctx.takePhoto({
      quality: 'normal',
      success: (res) => {
        let photoPath = res.tempImagePath
        that.setData({
          step: '2',
          photoPath: photoPath
        })
        // 上传未合并的图片,并给下一个页面
        that.uploadPhoto(photoPath, {}, )
      },
      fail: (err) => {
        wx.navigateTo({
          url: '/pages/home/home',
        })
      }
    })
  },
  drawImage(canvasId) {
    if (this.data.photoPath != '') {
      canvasId.drawImage(this.data.photoPath, 0, 0, 750, 920);
    }
    console.log(this.data.chooseImg)
    if (this.data.chooseImg != '') {
      canvasId.drawImage(this.data.chooseImg, (this.data.windowWidth - 256) / 2, (920 - 283) / 2, 256, 283);
    }
    canvasId.draw()
  },
  //返回
  back() {
    this.data.photoPath = ""
    this.data.showCanvas = wx.createCanvasContext("show");
    this.drawImage(this.data.showCanvas)
    this.setData({
      step: 1
    })
  },
  getreport(id) {
    console.log()
    var that = this
    wx.request({
      url: config.host + '/skinapi/getSkinReport?reportId=' + id, //that.data.id仅为示例，并非真实的接口地址
      method: 'POST',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: (res) => {
        if (res.data.data.valid == 0) {
          console.log("轮询中", res.data);
          if (times > 50) {
              // 返回首页
          wx.redirectTo({
            url: '/pages/home/home?opentc=1' + '&txt=' +'分析超时'+times,
          })
          } else {
            times=times+1
            setTimeout(that.getreport(id), 2000)
          }
        } else if (res.data.data.valid == 1) {
          console.log('上次传照片成功,跳转到结果分析页面')
          wx.redirectTo({
            url: '/pages/details/details'
          })
        } else if (res.data.data.valid == 2) {
          // 返回首页
          wx.redirectTo({
            url: '/pages/home/home?opentc=1' + '&txt=' +'测肤失败'
          })
        }
      }
    });
  },
  //上传照片
  uploadPhoto: function (tempFilePath, Account) {
    var that = this
    console.log('上传照片')
    publicrequest.upload_file(tempFilePath, Account, Service.queryupload, function (data) {
      console.log(data, 77)
      if (data.code == 200) {
        app.globalData.detailsId = data.data
        that.getreport(data.data);
      } else {}
    }, function (data) {
      if (data.code == 300) {
        setTimeout(function () {
          wx.redirectTo({
            //接着把重定向
            url: '/pages/home/home?opentc=1' + '&txt=' + data.msg,
          })
        }, 1000)
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {}
})