import * as echarts from '../../ec-canvas/echarts';
const app = getApp()
var config = require('../../utils/config');
import drawQrcode from '../../Lib/weapp.qrcode.min.js'
var dataList = [];
var indicatorclassList = [];
var indicatorData = [];
var Chart = null;
var ChartLd = null
var e = 80;
Page({
  data: {
    flag: false,
    floorstatus: false,
    showCanvas: '',
    cardInfoList: [],
    windowWidth: 0,
    windowHeight: 0,
    showtc: false,
    pay: false,
    paymonny: 0,
    linkimg: '',
    id: 0,
    page1: [],
    page2: [],
    page3: [],
    page4: [],
    page5: [],
    ec: {
      lazyLoad: true
    },
    ec2: {
      lazyLoad: true
    },
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    wx.hideShareMenu()// 禁用分享
    this.setData({
      id: app.globalData.detailsId?app.globalData.detailsId:408
    })
    this.getSystem()
    this.popup = this.selectComponent("#popup"); //获得popup组件
  },
  gotoGoods() {
    var that = this
    wx.navigateTo({ // 不销毁此页面的跳转
      url: '/pages/goods/goods?typeId =' + that.data.id
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.toopall = this.selectComponent('#module');
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      id: app.globalData.detailsId?app.globalData.detailsId:408
    })
    this.getData(); //获取数据
  },
  // 父页面调用子组件的方法
  childrenFunction() {
    this.module.childrenFunction()
  },
  // popup
  showPopup() {
    this.popup.showPopup();
  },
  //取消事件
  _error() {
    this.popup.hidePopup();
  },
  //确认事件
  _success() {
    this.popup.hidePopup();
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  payover() {
    // 判断是否已经支付
    var that = this
    wx.request({
      url: config.host + '/skinapi/getPayState?reportId=' + that.data.id, //that.data.id仅为示例，并非真实的接口地址
      method: 'post',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: (res) => {
        if (res.data.data == 1) {
          this.setData({
            showtc: false
          });
          that.initChart(); //更新数据
          that.initChartLd(); //更新数据
          wx.showToast({
            title: '下滑查看报告', // 标题
            icon: 'success', // 图标类型，默认success
            duration: 1500 // 提示窗停留时间，默认1500ms
          })
          that.setData({
            pay: true
          });
        } else if (res.data.data == 0) {
          that.setData({
            flag: true
          });
          that.popup.showPopup();
          const instance = this.selectComponent('.popup');
          console.log(instance, 12)
          setTimeout(function () {
            that.setData({
              flag: false
            })
          }, 1410)
        }
      }
    })
  },
  ishadpay() {
    var that = this
    wx.request({
      url: config.host + '/skinapi/getPayState?reportId=' + that.data.id, //that.data.id仅为示例，并非真实的接口地址
      method: 'post',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: (res) => {
        if (res.data.data == 0) {
          this.setData({
            showtc: true,
            pay: false
          });
          this.draw()
        } else if (res.data.data == 1) {
          wx.showToast({
            title: '下滑查看报告', // 标题
            icon: 'success', // 图标类型，默认success
            duration: 1500 // 提示窗停留时间，默认1500ms
          })
          that.setData({
            pay: true
          });
        }
      }
    })

  },
  touchStart(){},
  hiddentc() {
    this.setData({
      showtc: false
    });
    this.initChart(); //更新数据
    this.initChartLd(); //更新数据
  },
  draw() {
    console.log(this.data.cardInfoList[0].payUrl, 77)
    drawQrcode({
      x: 65,
      y: 20,
      canvasId: 'myQrcode',
      // ctx: wx.createCanvasContext('myQrcode'),
      typeNumber: 10,
      text: this.data.cardInfoList[0].payUrl,
      callback(e) {}
    })
  },
  getData() {
    Chart = null
    ChartLd = null
    indicatorData = []
    indicatorclassList = []
    dataList = []
    var that = this
      dataList = [{
        name: '游泳健身',
        value: 80,
        itemStyle: {
          normal: {
            color: 'rgba(204, 224, 247, 0.5)'
          }
        }
      }, {
        name: '学车考驾照',
        value: 20,
        itemStyle: {
          normal: {
            color: 'rgba(240, 243, 249, 0.5)'
          }
        }
      }]
      let sum = 0
      // 放置需要显示的图例(if you need)
      let legendData = []
      // 循环数据 累计sum值
      dataList.forEach(item => {
        sum += item.value * 1 // *1保证sum值为数值
      })
      // 给数据加上总数sum 通过颜色及透明度设置不显示
      dataList.push({
        name: '总数',
        value: 100,
        itemStyle: {
          normal: {
            color: 'rgba(0, 0, 0, 0)'
          }
        }
      })
    wx.request({
      url: config.host + '/skinapi/getSkinReport?reportId=' + that.data.id, //that.data.id仅为示例，并非真实的接口地址
      method: 'POST',
      data: {},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: (res) => {
        console.log("XXX", res.data);
        if (res.data.data.valid == 0) {
          console.log("轮询中", res.data);
          setTimeout(that.getData(), 1500)
        } else if (res.data.data.valid == 1) {
          that.setData({
            linkimg: res.data.data.page1.faceUrl,
            paymonny: res.data.data.page1.payAmount
          })
          let data = []
          data.push(res.data.data.page1)
          data.push(res.data.data.page2)
          data.push(res.data.data.page3)
          data.push(res.data.data.page4)
          data.push(res.data.data.page5)
          let t
          data.forEach((element, index) => {
            t = 'page' + (index + 1)
            element[t] = index + 1;
          })
          dataList[0].value = data[0].confidence
          dataList[1].value = 100 - data[0].confidence
          e = data[0].confidence
          that.setData({
            cardInfoList: data
          })

          that.initChart(); //初始化图表
          indicatorclassList = this.data.cardInfoList[0].radar
          indicatorData.push(this.data.cardInfoList[0].radar[0].confidence)
          indicatorData.push(this.data.cardInfoList[0].radar[1].confidence)
          indicatorData.push(this.data.cardInfoList[0].radar[2].confidence)
          indicatorData.push(this.data.cardInfoList[0].radar[3].confidence)
          indicatorData.push(this.data.cardInfoList[0].radar[4].confidence)
          indicatorData.push(this.data.cardInfoList[0].radar[5].confidence)
          console.log(indicatorData,indicatorclassList)
          this.clsz(1, this.data.cardInfoList[0].content.split("["))
          this.clsz(2, this.data.cardInfoList[1].data[0].content.split("["), this.data.cardInfoList[1].data[1].content.split("["))
          this.clsz(3, this.data.cardInfoList[2].data[0].content.split("["), this.data.cardInfoList[2].data[1].content.split("["))
          this.clsz(4, this.data.cardInfoList[3].data[0].content.split("["), this.data.cardInfoList[3].data[1].content.split("["))
          this.clsz(5, this.data.cardInfoList[4].data[0].content.split("["), this.data.cardInfoList[4].data[1].content.split("["))
          this.initChartLd();
        } else if (res.data.data.valid == 2) {
          // 返回首页
          wx.redirectTo({
            url: '/pages/home/home'
          })
        }

      }
    });
  },
  initChart() {
    this.echartsComponnet = this.selectComponent('#mychart-dom-gauge'); // 本来放在load中不过渲染要在绑定前
    this.echartsComponnet.init((canvas, width, height, dpr) => {
      Chart = echarts.init(canvas, null, {
        width: width,
        height: height,
        devicePixelRatio: dpr
      })
      this.setOption(Chart);
      return Chart;
    });
  },
  setOption(Chart) {
    Chart.clear(); // 清除
    Chart.setOption(this.getOption()); //获取新数据    
  },
  getOption() {
    // 指定图表的配置项和数据
    var option = {
      tooltip: {
        trigger: 'item',
        show: false
      },
      grid: {
        left: "0%",
        right: "0%",
        bottom: "0%"
      },
      title: {
        show: true,
        text: e + '分',
        x: 'center',
        y: 'center',
        textStyle: {
          fontSize: '20',
          color: '#000',
          fontWeight: 'bold'
        }
      },
      series: {
        name: '',
        type: 'pie',
        clickable:false,
        startAngle: 180, //开始角度 左侧角度
        radius: ['70%', '85%'],
        avoidLabelOverlap: true,
        hoverAnimation: false,
        label: {
          normal: {
            show: false,
            position: 'center'
          },
          emphasis: {
            show: false
          }
        },
        labelLine: {
          normal: {
            show: false
          }
        },
        data: dataList
      }
    }
    return option;
  },
  setOptionLd(ChartLd) {
    ChartLd.clear(); // 清除
    ChartLd.setOption(this.getOptionLd()); //获取新数据    
  },
  getOptionLd() {
    var optionLd = {
      tooltip: {
        trigger: 'axis',
        show:false,
      },
      grid: {
        left: "0%",
        right: "0%",
        bottom: "0%",
        //            width: "750px",
        //          height: "350px"
      },
      radar: [{
        splitNumber: 4, // 雷达图圈数设置
        indicator: indicatorclassList,
        name: {
          formatter: '{value}',
          textStyle: {
            color: '#cbb2a1',
            fontSize: 22 // 文字颜色
          }
        },
        //雷达图背景的颜色，在这儿随便设置了一个颜色，完全不透明度为0，就实现了透明背景
        splitArea: {
          show: false
        },
        radius: 170,
        center: ['50%', '53%'],
      }],
      series: [{
        clickable:false,
        type: 'radar',
        tooltip: {
          trigger: 'item',
          show:false,
        },
        areaStyle: {},
        data: [{
          value: indicatorData,
          // 设置区域边框和区域的颜色
          itemStyle: {
            normal: {
              color: 'rgba(148,183,250,.5)',
              lineStyle: {
                color: 'rgba(241,241,241,.5)',
              },
            },
          },
        }]
      }]
    }
    return optionLd;
  },
  initChartLd() {
    this.echartsLdComponnet = this.selectComponent('#mychart-dom-leida'); // 本来放在load中不过渲染要在绑定前
    this.echartsLdComponnet.init((canvas, width, height, dpr) => {
      ChartLd = echarts.init(canvas, null, {
        width: width,
        height: height,
        devicePixelRatio: dpr
      })
      this.setOptionLd(ChartLd);
      return ChartLd;
    });
  },
  clsz(page, a, b) { // 处理数据
    var all = []
    if (a) {
      var arrya = a
      var rega = /(.*?)\]/g;
      for (let i = 0; i < arrya.length; i++) {
        let n = arrya[i].match(rega);
        if (n != null) {
          let c = arrya[i].split(']');
          arrya[i] = c;
        } else {
          arrya[i] = new Array("", arrya[i]);
        }
      }
      all.push(arrya)
    }
    if (b) {
      var arryb = b
      var regb = /(.*?)\]/g;
      for (let i = 0; i < arryb.length; i++) {
        let n = arryb[i].match(regb);
        if (n != null) {
          let c = arryb[i].split(']');
          arryb[i] = c;
        } else {
          arryb[i] = new Array("", arryb[i]);
        }
      }
      all.push(arryb)
    }
    page = 'page' + page
    this.setData({
      [page]: all
    })
  },
  getSystem() {
    let that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          windowWidth: res.windowWidth,
          windowHeight: res.windowHeight
        })
      },
    })
  },
  // 获取滚动条当前位置
  onPageScroll: function (e) {
    console.log(e, this.data.pay)
    if (e.scrollTop > 100) {
      this.setData({
        floorstatus: true
      });
    } else {
      this.setData({
        floorstatus: false
      });
    }
  },
  //回到顶部
  goTop: function (e) { // 一键回到顶部
    if (wx.pageScrollTo) {
      wx.pageScrollTo({
        scrollTop: 0
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {}
})